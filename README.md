# Archaeosaurus

Archaeosaurus est le [thésaurus de la Grotte Chauvet-Pont-d'Arc](https://opentheso.huma-num.fr/opentheso/?idt=th312) en cours de développement. Ce thésaurus est développé avec l'instance [Opentheso](https://opentheso.huma-num.fr/) hébergée par la [TGIR Huma-Num](https://www.huma-num.fr/). Les différentes versions peuvent être téléchargées au format rdf sur le [dépôt git du projet](https://gitlab.huma-num.fr/grotte-chauvet/archaeosaurus).

## Objectif

Le thésaurus de la Grotte Chauvet-Pont-d'Arc fournit un vocabulaire contrôlé de concepts scientifiques pertinents pour la mission et le travail de l'équipe scientifique. Ce vocabulaire contrôlé aide à catégoriser les informations de l'équipe et permet aux personnes et aux ordinateurs de trouver des informations qui ont des caractéristiques communes. Ce thésaurus ne contient pas de vocabulaire taxinomique pour la classification du vivant : le [référentiel taxonomique TaxRef](https://inpn.mnhn.fr/accueil/recherche-de-donnees/especes/) doit être utilisé pour l'identification des espèces.

## Contenu

Le thésaurus de la Grotte Chauvet-Pont-d'Arc comporte les catégories de premier niveau suivantes, utilisées pour regrouper les concepts scientifiques :

- **choses :** instances discrètes et identifiables, documentées en tant qu'unités uniques et sujettes d'une étude.
- **données :** types de données et de documents, représentations de l'information.
- **matériaux :** nature et types des matériaux constituant les choses ou support de ces dernières.
- **méthodes :** techniques, méthodes, procédures ou stratégies de recherche, de gestion, de collecte ou d'analyse de l'information scientifique.
- **périodes :** intervalles temporels définis à des fins de chronologie par la réunion de phénomènes physiques cohérents, de manifestations culturelles ou matérielles se produisant dans le temps et l'espace.
- **phénomènes :** évènements perceptibles affectant les choses, leurs constituants ou leurs supports.
- **sciences :** activités d'élaboration de la connaissance distinctes de par la nature de leur sujet d'étude et de leurs méthodes.

## Notes

- Éviter les usages métonymiques (remplacer le tout par sa partie, par exemple "céramique" est un matériau et non un objet).
- Éviter la confusion entre "spectroscopie" et "spectrométrie" (préféré) : "La spectroscopie est la science qui traite des interactions de différents types de rayonnement avec la matière. [...] La spectrométrie, quant à elle, concerne l'instrumentation et les mesures pour des études spectroscopiques." (Techniques de l'Ingénieur).

## Contribuer

Le thésaurus de la Grotte Chauvet-Pont-d'Arc est maintenu par Nicolas Frerebeau. Le signalement d'erreurs ou les propositions d'améliorations ou de modifications peuvent être soumises en ouvrant un [ticket sur le dépôt git du projet](https://gitlab.huma-num.fr/grotte-chauvet/archaeosaurus/-/issues).

